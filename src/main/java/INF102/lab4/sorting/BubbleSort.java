package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int n = list.size();
        boolean swapped;
        
        for (int i = 0; i < n-1; i++) {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++) {
                T first = list.get(j);  
                T second = list.get(j+1); 

                if (first.compareTo(second) > 0) {
                    list.set(j, second); 
                    list.set(j + 1, first);  
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
    }
}
