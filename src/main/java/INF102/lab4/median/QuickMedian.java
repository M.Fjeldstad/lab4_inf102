package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int midIndex = listCopy.size() / 2;
        return quickSelect(listCopy, midIndex);
    }

    

    public <T extends Comparable<? super T>> T quickSelect(List<T> list, int k) {
		Random rnd = new Random();
        
        if (list.size() == 1) {
            return list.get(0);
        }

        T pivot = list.get(rnd.nextInt(list.size()));
		
        List<T> first = new ArrayList<>();
        List<T> second = new ArrayList<>();
        List<T> pivots = new ArrayList<>();

        for (T t : list) {
            int comparator = t.compareTo(pivot);
            if (comparator < 0) {
                first.add(t);
            } else if (comparator > 0) {
                second.add(t);
            } else {
                pivots.add(t);
            }
        }

        if (k < first.size()) {
            return quickSelect(first, k);
        } else if (k < first.size() + pivots.size()) {
            return pivots.get(0);
        } else {
            return quickSelect(second, k - first.size() - pivots.size());
        }
    }
}
