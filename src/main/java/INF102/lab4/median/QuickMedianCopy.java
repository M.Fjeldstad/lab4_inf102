package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedianCopy implements IMedian {

    MedianFinder<Integer> myMedianFinder = new MedianFinder<>(); // Assuming you want to find the median of integers

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Create a copy of the original list

        for (T element : listCopy) {
            myMedianFinder.add((Integer) element); // Add each element to the MedianFinder
        }

        return (T) myMedianFinder.getMedian(); // Get the median from the MedianFinder
    }
}
